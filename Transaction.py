class Transaction:
    __type = None
    __value = 0
    __units = 0
    __budget = 0

    def __init__(self, type, value, units, budget):
        self.__type = type
        self.__units = units
        self.__value = value
        self.__budget = budget

    @property
    def type(self):
        return self.__type

    def __str__(self):
        return "Type: " + str(self.__type) + " Value: " + str(self.__value) + " Units: " + str(self.__units) +\
               " Budget: " + str(self.__budget)

    def __repr__(self):
        return "Type: " + str(self.__type) + " Value: " + str(self.__value) + " Units: " + str(self.__units) +\
               " Budget: " + str(self.__budget)

    def __iter__(self):
        return iter([self.__type, self.__value, self.__units, self.__budget])
