import csv

import matplotlib.pyplot as plt

from Day import Day


def get_data(filename):
    day_list = []
    with open(filename, "r") as f:
        reader = csv.reader(f)
        for row in reader:
            day_list.append(Day(row[0], row[1]))
    day_list = sorted(day_list)
    return day_list


def exponential_moving_average(value_list, curr_day, day_count):
    a = 0.
    b = 0.
    one_minus_alpha = 1 - (2 / (len(value_list) + 1))
    for i in range(day_count + 1):
        temp = one_minus_alpha ** i
        if curr_day - i > 0:
            a += temp * value_list[curr_day - i - 1]
        else:
            a += temp * value_list[0]
        b += temp
    return a / b


def moving_average_convergence_divergence(value_list, curr_day):
    return exponential_moving_average(value_list, curr_day + 1, 12) - exponential_moving_average(value_list,
                                                                                                 curr_day, 26)


def macd_signal(value_list, curr_day):
    return exponential_moving_average(value_list, curr_day + 1, 9)


class MACDGenerator:
    __filename = None
    __day_list = []
    __date_list = []
    __value_list = []
    __macd = []
    __signal = []

    def __init__(self, filename):
        self.__filename = filename
        self.__day_list = get_data(self.__filename)
        self.__value_list = [l.value for l in self.__day_list]
        for i in range(len(self.__day_list)):
            self.__day_list[i].macd = moving_average_convergence_divergence(self.__value_list, i)
        macd = [l.macd for l in self.__day_list]
        for i in range(len(self.__day_list)):
            self.__day_list[i].signal = macd_signal(macd, i)
        self.__date_list = [l.date for l in self.__day_list]
        self.__macd = [l.macd for l in self.__day_list]
        self.__signal = [l.signal for l in self.__day_list]

    def get_filename(self):
        return self.__filename

    def get_day_list(self):
        return self.__day_list

    def get_value_plot(self):
        fig, axs = plt.subplots(1, 1)
        fig.subplots_adjust(wspace=0.5)
        fig.suptitle('Plot generated from file: ' + self.__filename)
        axs.set_title('Values over time')
        axs.plot(self.__date_list, self.__value_list, label='Values')
        axs.set_xlabel('Date [yyyy-mm]')
        axs.set_ylabel('Value [$]')
        fig.autofmt_xdate()
        return fig

    def get_macd_and_signal_plot(self):
        fig, axs = plt.subplots(1, 1)
        fig.subplots_adjust(wspace=0.5)
        fig.suptitle('Plot generated from file: ' + self.__filename)
        axs.set_title('MACD and SIGNAL')
        axs.set_xlabel('Date [yyyy-mm]')
        axs.set_ylabel('Value [U]')
        axs.plot(self.__date_list, self.__macd, label='MACD')
        axs.plot(self.__date_list, self.__signal, label='SIGNAL')
        axs.legend(loc='best')
        fig.autofmt_xdate()
        return fig

    def get_figure(self):
        fig, axs = plt.subplots(1, 2)
        fig.subplots_adjust(wspace=0.5)
        fig.suptitle('Plot generated from file: ' + self.__filename)
        axs[0].set_title('Values over time')
        axs[0].plot(self.__date_list, self.__value_list, label='Values')
        axs[0].set_xlabel('Date [yyyy-mm]')
        axs[0].set_ylabel('Value [$]')
        axs[1].set_title('MACD and SIGNAL')
        axs[1].set_xlabel('Date [yyyy-mm]')
        axs[1].set_ylabel('Value [U]')
        axs[1].plot(self.__date_list, self.__macd, label='MACD')
        axs[1].plot(self.__date_list, self.__signal, label='SIGNAL')
        axs[1].legend(loc='best')
        fig.autofmt_xdate()
        return fig

    def get_macd(self):
        return self.__macd

    def get_signal(self):
        return self.__signal

    def get_values(self):
        return self.__value_list
