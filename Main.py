# Example usage: python Main.py LTCPrice.csv
import sys

from MACDGenerator import MACDGenerator
from Simulation import Simulation

if __name__ == "__main__":
    if len(sys.argv) == 1:
        raise ValueError('Please provide valid filename')
    filename = sys.argv[1].split('.')[0]
    generator = MACDGenerator(sys.argv[1])
    fig = generator.get_figure()
    values_plot = generator.get_value_plot()
    values_plot.savefig(filename+".png")
    macd_and_signal_plot = generator.get_macd_and_signal_plot()
    macd_and_signal_plot.savefig(filename+"MACD_Signal.png")
    fig.savefig(filename+"Plot.png")
    print("\nStarting simulation on "+filename)
    simulation = Simulation(1000, generator.get_values(), generator.get_macd(), generator.get_signal())
    simulation.save_transactions(filename)
