import datetime


class Day:
    date = None
    value = None
    signal = None
    macd = None

    def __init__(self, date, value):
        year, month, day = date.split('-')
        self.date = datetime.datetime(int(year), int(month), int(day))
        self.value = float(value)

    def __lt__(self, other):
        return self.date < other.date

    def __eq__(self, other):
        return self.date == other.date
