import csv

from Transaction import Transaction


class Simulation:
    __starting_budget = 0
    __ending_budget = 0
    __transactions = []
    __curr_units = 0
    __curr_budget = 0

    def __init__(self, starting_units, value_list, macd_list, signal_list):
        self.__curr_units = starting_units
        self.__curr_budget = self.__starting_budget = starting_units * value_list[0]
        print("Starting with " + str(starting_units) + " units")
        self.__curr_units = 0
        self.__transactions.append(Transaction("SELL", value_list[0], 0, self.__starting_budget))
        print(self.__transactions[-1])
        for i in range(1, len(macd_list)):
            if macd_list[i] > signal_list[i] and macd_list[i - 1] <= signal_list[i - 1] and macd_list[i] > 0:
                tmp = int(self.__curr_budget / value_list[i])
                if tmp > 0:
                    self.__curr_budget -= tmp * value_list[i]
                    self.__curr_units += tmp
                    self.add_transaction("BUY", value_list[i])
                else:
                    self.add_transaction("HOLD", value_list[i])
            elif macd_list[i] < signal_list[i] and macd_list[i - 1] >= signal_list[i - 1] and macd_list[i] < 0:
                if self.__curr_units > 0:
                    self.__curr_budget += self.__curr_units * value_list[i]
                    self.__curr_units = 0
                    self.add_transaction("SELL", value_list[i])
                else:
                    self.add_transaction("HOLD", value_list[i])
            else:
                self.add_transaction("HOLD", value_list[i])
            if self.__transactions[-1].type != "HOLD":
                print(self.__transactions[-1])

        self.__ending_budget = self.__curr_budget
        if self.__curr_units > 0:
            self.__ending_budget += self.__curr_units * value_list[-1]
        print("Ending with budget: " + str(self.__ending_budget))
        print("Income percent: " + str((self.__ending_budget - self.__starting_budget) / self.__starting_budget * 100) + "%")

    def save_transactions(self,  file_index):
        filename = str(file_index) + "_transactions.csv"
        with open(filename, 'w', newline='') as csvFile:
            writer = csv.writer(csvFile)
            writer.writerows(self.__transactions)
        csvFile.close()

    def add_transaction(self, type, value):
        self.__transactions.append(Transaction(type, value, self.__curr_units, self.__curr_budget))

